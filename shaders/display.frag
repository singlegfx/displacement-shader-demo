#version 460 core
precision highp float;

layout(location = 0) out vec4 FragColor;

in vec2 TexCoord;
in vec2 Coord;

uniform sampler2D tex[8];
uniform sampler2D displaceTex[8];

uniform uint enabledUnits = 1;
uniform float multiplier = 1.0;
const float zeroOffset = 127.0/255.0;

void main(){

    for(int i = 0; i < enabledUnits; i++){
        vec2 displacement = texture(displaceTex[i], TexCoord).rg - vec2(zeroOffset, zeroOffset);
        vec4 outColor = texture(tex[i], TexCoord - displacement * multiplier);
        FragColor = mix(FragColor, outColor, outColor.a);
    }

}


