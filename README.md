# Displacement Shader Demo

This is an example program to show how to use the SimpleGL library.
In addition to that this program also shows how Displacement maps work by being able to switch between them and adjust their strength.

