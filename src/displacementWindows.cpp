#include "displacementWindows.hpp"

using namespace SimpleGFX::SimpleGL;
using namespace std::literals;
namespace fs = std::filesystem;

mainWindow::mainWindow(): window("main window") {
    //load the display shader and set the default values
    try{
        shader = std::make_shared<Shader>("shaders/display.vert","shaders/display.frag");
        std::vector<int> Texunits {0,1,2, 3, 4, 5, 6, 7};
        std::vector<int> Displaceunits {8,9,10,11,12,13,14,15};
        shader->use();
        shader->setUniform("tex", Texunits);
        shader->setUniform("displaceTex", Displaceunits);
        shader->setUniform("enabledUnits", (size_t) 2);
    }catch(const std::exception& e){
        std::throw_with_nested(std::runtime_error("cannot init display shader"));
    }
    
    //load displacement texture
    for(uint64_t i = 0; i < 13;i++){
        std::stringstream URI{};
        URI << "displacement-" << i << ".tif";
        auto tex = std::make_shared<SimpleGFX::SimpleGL::texture>(fs::path("texture")/URI.str());
        displacements.emplace_back(tex);
    }
    
    //load the images
    bg = std::make_shared<texture>( fs::path{"texture/background.png"} );
    outputTexture = std::make_shared<texture>( "output"s );
    outputTexture->createFramebuffer(bg->getSize());
    
    showWindow = true;
}

void mainWindow::content(){

    //load the output buffer
    outputTexture->loadFramebuffer();
    
    //get the shader and set the vars
    
    shader->use();
    
    glm::mat4 transform{1.0f};
    auto orth = glm::ortho(
        -1.0f,1.0f,
        -1.0f,1.0f,
        -10.0f,10.0f
    );
    shader->setUniform("transform", orth*transform);
    shader->setUniform("multiplier", displacementStrength);
    
    bg->bind(0);
        
    imguiHandler::getDarkenTexture(40)->bind(1);
    displacements[activeDisplacementTexture]->bind(8);
    
    imguiHandler::drawRect();
    outputTexture->displayImGui();

}

selectorWindow::selectorWindow(std::shared_ptr<mainWindow> _mainWin):window("displacement selector"), mainWin{_mainWin}{
    showWindow = false;
}

void selectorWindow::content(){
    for(int i = 0; i < mainWin->displacements.size();i++){
        std::stringstream URI{};
        URI << "displacement " << i;
        ImGui::RadioButton(URI.str().c_str(), &mainWin->activeDisplacementTexture, i);
    }
}

mapWindow::mapWindow(std::shared_ptr<mainWindow> _mainWin):window("displacement map"), mainWin{_mainWin}{
    showWindow = false;
}

void mapWindow::content(){
    mainWin->displacements[mainWin->activeDisplacementTexture]->displayImGui({512,512});
}

strengthWindow::strengthWindow(std::shared_ptr<mainWindow> _mainWin):window("diplacement multiplier selection"), mainWin{_mainWin}{
    showWindow = false;
}

void strengthWindow::content(){
    ImGui::SliderFloat("displacement multiplier", &mainWin->displacementStrength, 0.0f, 2.0f);
}
