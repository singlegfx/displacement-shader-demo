#pragma once

#include <exception>
#include <iostream>
#include <thread>
#include <memory>

#include "imguiHandler.hpp"
#include "shader.hpp"
#include "window.hpp"
#include "imguiHandler.hpp"

#include "shader.hpp"
#include "texture.hpp"

/*
 * All classes get a foreward declaration here.
 * This is needed to mark them as friedn class in the mainWindow before they
 * are even declared.
 * 
 * All classes in this file are windows that can be displayed.
 * They all inherit their behavior from the general window class defined in
 * window.hpp. This means they all have a constructor which sets the window name
 * and loads all needed data for that window and a protected content function.
 * The content function contains the content that will be displayed if the window
 * is drawn and visible. All of the logic for opening, closing, resizeing and merging
 * windows is part of the general window class. Each window can be marked as closable
 * in the constructor (which is the default). The mainWindow is not closable because
 * that one is always needed.
 */
class selectorWindow;
class mapWindow;
class strengthWindow;

/**
 * @brief the main window contains the background image modified by the displacement map
 *
 * The other window classes are marked as friend classes, so that they can access the
 * protected variables of the main window since it holds all the shaders, textures and
 * parameters.
 * 
 */
class mainWindow:public SimpleGFX::SimpleGL::window{
  friend class selectorWindow;
  friend class mapWindow;
  friend class strengthWindow;
  protected:
    std::shared_ptr<SimpleGFX::SimpleGL::Shader> shader;
    std::shared_ptr<SimpleGFX::SimpleGL::texture> bg;
    std::shared_ptr<SimpleGFX::SimpleGL::texture> outputTexture;
    std::vector<std::shared_ptr<SimpleGFX::SimpleGL::texture>> displacements;
    
    int activeDisplacementTexture = 0;
    float displacementStrength = 0.1f;
    
    void content() override;
  public:
    mainWindow();
};

/**
 * @brief the selector window is where the user can select the active displacement map
 * 
 */
class selectorWindow:public SimpleGFX::SimpleGL::window{
protected:
    std::shared_ptr<mainWindow> mainWin;
    void content() override;
public:
    selectorWindow(std::shared_ptr<mainWindow> _mainWin);
};

/**
 * @brief the mapWindow is where the currently active displacement map can be viewed
 */
class mapWindow:public SimpleGFX::SimpleGL::window{
protected:
    std::shared_ptr<mainWindow> mainWin;
    void content() override;
public:
    mapWindow(std::shared_ptr<mainWindow> _mainWin);
};

/**
 * @brief the strength window is where the displacement shader strength can be adjusted
 */
class strengthWindow:public SimpleGFX::SimpleGL::window{
protected:
    std::shared_ptr<mainWindow> mainWin;
    void content() override;
public:
    strengthWindow(std::shared_ptr<mainWindow> _mainWin);
};
