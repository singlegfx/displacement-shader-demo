#include "displacementWindows.hpp"

using namespace SimpleGFX::SimpleGL;

int main(int argc, char* argv[]){
    
    #ifdef setenv
        setenv( "MESA_DEBUG", "", 0 );
    #endif
    
    std::shared_ptr<mainWindow> mainWin;
    std::shared_ptr<selectorWindow> selectorWin;
    std::shared_ptr<mapWindow> mapWin;
    std::shared_ptr<strengthWindow> strengthWin;

    //load the imguiHandler and all the windows
    try{
        imguiHandler::init("displacement shader demo");
        mainWin = std::make_shared<mainWindow>();
        selectorWin = std::make_shared<selectorWindow>(mainWin);
        mapWin = std::make_shared<mapWindow>(mainWin);
        strengthWin = std::make_shared<strengthWindow>(mainWin);
    }catch(const std::exception& e){
        SimpleGFX::Helper::print_exception(e);
        return 1;
    }
    
    //the basic window loop
    //In this loop all windows are updated and drawn.
    //This runs until the program should be terminate,
    //which happens when the x button on the top of the
    //window is pressed.
    bool shouldClose = false;
    while(!shouldClose){
        if(imguiHandler::shouldTerminate()){
            shouldClose=true;
        }
        
        //All draw calls need to happen between the startRender
        //and endRender functions.
        imguiHandler::startRender();
        
            selectorWin->draw();
            mapWin->draw();
            strengthWin->draw();
            mainWin->draw();
        
        imguiHandler::endRender();
        
    }
    
    return 0;
}

